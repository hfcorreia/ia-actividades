;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Procura Profundidade Primeiro
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; Arvore
(defun pppa (formulacao-problema)
  (procura-arvore formulacao-problema #'pilha-inserir #'procura-profundidade))

;;; Grafo
(defun pppg (formulacao-problema)
  (procura-grafo formulacao-problema #'pilha-inserir #'procura-profundidade))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Procura Largura Primeiro
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; Arvore
(defun plpa (formulacao-problema)
  (procura-arvore formulacao-problema #'fila-inserir #'procura-largura :largura t))

;;; Grafo
(defun plpg (formulacao-problema)
  (procura-grafo formulacao-problema #'fila-inserir #'procura-largura :largura t))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Procura Profundidade Limitada Primeiro
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; Arvore
(defun ppla (formulacao-problema limite)
  (let ((resultado nil))
    (setf resultado (procura-arvore formulacao-problema #'pilha-inserir #'procura-profundidade :limite limite))
    (when (equal resultado 'cutoff)
      (setf resultado nil))
    resultado))

;;; Grafo
(defun pplg (formulacao-problema limite)
  (let ((resultado nil))
    (setf resultado (procura-grafo formulacao-problema #'pilha-inserir #'procura-profundidade :limite limite))
    (when (equal resultado 'cutoff)
      (setf resultado nil))
    resultado))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Procura Profundidade Iterativa Primeiro
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; Arvore
(defun ppia (formulacao-problema)
  (let ((resultado nil))
    (loop for l from 0 do
      (setf resultado (procura-arvore formulacao-problema #'pilha-inserir #'procura-profundidade :limite l))
      (when (null resultado)
        (return nil))
      (when (not (equal resultado 'cutoff))
        (return resultado)))))

;;; Grafo
(defun ppig (formulacao-problema)
  (let ((resultado nil))
    (loop for l from 0 do
      (setf resultado (procura-grafo formulacao-problema #'pilha-inserir #'procura-profundidade :limite l))
      (when (null resultado)
        (return nil))
      (when (not (equal resultado 'cutoff))
        (return nil)))
    resultado))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Procura Profundidade Custo Uniforme
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; Arvore
(defun pcua (formulacao-problema)
  (procura-arvore formulacao-problema #'pilha-inserir-ordenado #'procura-profundidade :custo t :custo-caminho t))

;;; Grafo
(defun pcug (formulacao-problema)
  (procura-grafo formulacao-problema #'pilha-inserir-ordenado #'procura-profundidade :custo t :custo-caminho t))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Procura Gananciosa
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; Arvore
(defun pga (formulacao-problema heuristica)
  (procura-arvore formulacao-problema #'pilha-inserir-ordenado #'procura-profundidade :custo t :gananciosa t :heuristica heuristica))

;;; Grafo
(defun pgg (formulacao-problema heuristica)
  (procura-grafo formulacao-problema #'pilha-inserir-ordenado #'procura-profundidade :custo t :gananciosa t :heuristica heuristica))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Procura A*
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; Arvore
(defun pA*a (formulacao-problema heuristica)
  (procura-arvore formulacao-problema #'pilha-inserir-ordenado #'procura-profundidade :custo t :heuristica heuristica :custo-caminho t))

;;; Grafo
(defun pA*g (formulacao-problema heuristica)
  (procura-grafo formulacao-problema #'pilha-inserir-ordenado #'procura-profundidade :custo t :heuristica heuristica :custo-caminho t))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Procura Melhor Primeiro Recursiva
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun procura-rbfs (problema no limite heuristica)
  (let ((sucessores nil)
        (no-gerado nil)
        (estado-no (no-estado no))
        (avaliado nil)
        (melhor nil)
        (alternativa 0)
        (resultado))
    (if (funcall (problema-solucao? problema) estado-no)
      (cons estado-no 0)
      (block looping
        (dolist (accao (funcall (problema-accoes problema) estado-no))
          (setf no-gerado (no-filho problema no accao))
          (setf avaliado (avaliacao no-gerado :heuristica heuristica :custo t))
          (setf (no-avaliacao no-gerado) (max (no-avaliacao no) (no-avaliacao avaliado)))
          (setf sucessores (pilha-inserir-ordenado no-gerado sucessores)))
        (when (null sucessores)
          (return-from looping (cons nil most-positive-fixnum)))
        (loop
          (setf melhor (pop sucessores))
          (when (> (no-avaliacao melhor) limite)
            (return-from looping (cons nil (no-avaliacao melhor))))
          (if (not (null sucessores))
            (setf alternativa (no-avaliacao (first sucessores)))
            (setf alternativa most-positive-fixnum))
          (setf resultado (procura-rbfs problema melhor (min limite alternativa) heuristica))
          (setf (no-avaliacao melhor) (cdr resultado))
          (setf sucessores (pilha-inserir-ordenado melhor sucessores))
          (if (not (null (car resultado)))
            (return-from looping resultado)))))))

;;; Arvore
(defun rbfs (formulacao-problema heuristica)
  (car (procura-rbfs formulacao-problema (make-no :estado (problema-estado-inicial formulacao-problema) :nivel 0 :custo-caminho 0 :avaliacao 0) most-positive-fixnum heuristica)))
