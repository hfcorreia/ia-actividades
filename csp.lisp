(defun compara-tarefas (t1 t2)
  (< (length t1) (length t2)))

(defun compara-alternativas (alt1 alt2)
  (> (length alt1) (length alt2)))

(defun faz-csp (lista-tarefas)
  (let ((tarefas (make-hash-table :test #'equal))
        (alternativas (make-hash-table :test #'equal))
        (n 0)
        (restricoes nil)
        (tarefas-ordenadas (sort (copy-list lista-tarefas) #'compara-tarefas)))

    (dolist (tarefa tarefas-ordenadas)
      (setf tarefa (sort tarefa #'compara-alternativas))
      (setf (gethash n tarefas) nil)
      (setf (gethash n alternativas) tarefa)
      (incf n))

    (setf restricoes
      (lambda (atribuicao val)
        (let ((subtarefas (make-hash-table :test #'equal)))
          (block teste
            (loop for n being the hash-keys of atribuicao using (hash-value alt) do
              (dolist (sub alt)
                (setf (gethash (cons (first sub) (second sub)) subtarefas) (third sub))))
            (dolist (sub val)
              (let ((valor (gethash (cons (first sub) (second sub)) subtarefas)))
               (if (and valor (not (equal (third sub) valor)))
                 (return-from teste nil))))
            t))))

    (make-csp :variaveis tarefas :dominios alternativas :restricoes restricoes)))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Backtrack aux funtions
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun constroi-resultado (hash)
  (let ((resultado nil))
    (if hash
      (loop for n being the hash-keys of hash using (hash-value valor) do
            (setf resultado (append (list valor) resultado))))
    resultado))

(defun completa? (hash csp)
  (= (hash-table-count hash)
     (hash-table-count (csp-variaveis csp))))

(defun nao-atribuidos (csp) 
  (loop for n being the hash-keys of (csp-variaveis csp) using (hash-value valor) do
        (if (null valor)
          (return-from nao-atribuidos n))))

(defun valores-dominio (var csp) 
  (gethash var (csp-dominios csp)))

(defun inferencia (csp atribuicao)
  (let ((dominios (csp-dominios csp))
        (variaveis (csp-variaveis csp))
        (dominio nil))
    
    (loop for n being the hash-keys of variaveis using (hash-value valor) do
      (when (not valor)
        (setf dominio (gethash n dominios))
        (dolist (alt dominio)
          (when (not (funcall (csp-restricoes csp) atribuicao alt))
            (setf dominio (pilha-remove-elemento alt dominio))))
        (when (not dominio)
          (return-from inferencia nil))
        (setf (gethash n dominios) dominio)))
    (setf (csp-dominios csp) dominios)
    csp))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Backtrack funtion
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun backtrack (atribuicao csp)
  (let ((inferencias nil)
        (resultado nil)
        (var nil)
        (csp-backup csp))
    (if (completa? atribuicao csp)
      (return-from backtrack atribuicao)
      (progn
        (setf var (nao-atribuidos csp))
        (dolist (val (valores-dominio var csp)) ;leva na atribuicao
          (if (funcall (csp-restricoes csp) atribuicao val)
            (progn
              (setf (gethash var atribuicao) val)
              (setf (gethash var (csp-variaveis csp)) val)
              (setf inferencias (inferencia csp atribuicao))
              (when inferencias
                (setf resultado (backtrack atribuicao inferencias))
                (when resultado
                  (return-from backtrack resultado)))))
          (remhash var atribuicao)
          (setf csp csp-backup)
          (setf (gethash var (csp-variaveis csp)) nil))))
    nil))
        

(defun psr (csp)
    (constroi-resultado (backtrack (make-hash-table :test #'equal) csp)))
