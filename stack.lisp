;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Stack functions
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun pilha-inserir (gerado lista)
  (append (list gerado) lista))

;(defun pilha-inserir-ordenado (elemento lista &optional avalicao)
;  (cond ((null lista)
;          (list elemento))
;        ((>= (no-custo-caminho (car lista)) (no-custo-caminho elemento))
;          (cons elemento (cons (car lista) (cdr lista))))
;        ((< (no-custo-caminho (car lista)) (no-custo-caminho elemento))
;          (cons (car lista) (pilha-inserir-ordenado elemento (cdr lista))))))

(defun pilha-inserir-ordenado (elemento lista)
  (cond ((null lista)
          (list elemento))
        ((>= (no-avaliacao (car lista)) (no-avaliacao elemento))
          (cons elemento lista))
        (t (cons (car lista) (pilha-inserir-ordenado elemento (cdr lista))))))

(defun pilha-existe-elemento? (elemento lista)
  (cond ((null lista) nil)
        ((equal (car lista) elemento) t)
        (t (pilha-existe-elemento? elemento (cdr lista)))))

(defun pilha-remove-elemento (elemento lista)
  (cond ((null lista) nil)
        ((equal elemento (car lista)) (cdr lista))
        (t (cons (car lista) (pilha-remove-elemento elemento (cdr lista))))))


(defun pilha-insere-ordenado (elemento lista)
  (cond ((null lista)
          (list elemento))
        ((>= (car lista) elemento)
          (cons elemento lista))
        ((< (car lista) elemento)
          (cons (car lista) (pilha-insere-ordenado elemento (cdr lista))))))

(defun ordena-1-a-1 (lista)
  (let ((resultado nil))
    (dolist (e lista)
      (setf resultado (pilha-insere-ordenado e resultado)))
    resultado))



