(defun formulacao-problema1 (lista-tarefas)
  (let ((tabelinha (make-hash-table :test #'equal)))
  ;Cria um novo problema baseado numa lista de tarefas
  (make-problema
    :estado-inicial '()

    :solucao?
    (lambda (estado)
      (if (= (length estado) (length lista-tarefas))
        t
        nil))

    :accoes
    (lambda (estado)
      (let ((accoes nil)
            (provisoria nil)
            (alternativas (make-hash-table :test #'equal))
            (tabela (make-hash-table :test #'equal)))


        (dolist (alt estado)
          (setf (gethash alt alternativas) t)
          (dolist (sub alt)
            (setf (gethash (cons (first sub)
                                 (second sub))
                           tabela)
                  (third sub))))
        (dotimes (n (length lista-tarefas))
          (block looping
            (dolist (alt (nth n lista-tarefas))
              (setf (gethash alt tabelinha) n)
              (when (gethash alt alternativas)
                (setf provisoria nil)
                (return-from looping nil))
              (block subtarefas
                (dolist (sub alt)
                  (if (gethash (cons (first sub) (second sub)) tabela)
                    (when (not (equal (third sub) (gethash (cons (first sub) (second sub)) tabela)))
                      (return-from subtarefas nil))))
                (setf provisoria (append provisoria (list alt)))))
            (setf accoes (append accoes provisoria)))
            (setf provisoria nil))
        accoes))

    :resultado
    (lambda (estado accao)
      (let ((novo-estado nil)
            (lista nil))

        (dolist (alt estado)
          (setf lista (append lista (list (list alt (gethash alt tabelinha))))))

        (setf lista (append lista (list (list accao (gethash accao tabelinha)))))

        (sort lista #'(lambda (el1 el2) (< (second el1) (second el2))))

        (dolist (alt lista)
          (setf novo-estado (append novo-estado (list (first alt)))))
        novo-estado))

    :custo-caminho 
    (lambda (estado accao)
      (let ((valor 0)
            (tabela (make-hash-table :test #'equal)))
        (if (not (null estado))
          (progn
            (dolist (alt estado)
              (dolist (sub alt)
                (setf (gethash (cons (first sub)
                                     (second sub))
                               tabela)
                      (third sub))))

            (dolist (sub accao)
              (if (not (gethash (cons (first sub) (second sub)) tabela))
                (incf valor))))
          (setf valor (length accao)))
        valor)))))
