;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Test funtions
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun criar-teste (tarefas alternativas subtarefas)
  (let ((result '())) 
    (loop for i from 0 below tarefas do
          (let ((tarefa '()))
            (loop for j from 0 below (+ 1 (random alternativas)) do
                  (let ((alt '()))
                    (loop for h from 0 below (+ 1 (random subtarefas)) do
                          (let ((hash (make-hash-table :test #'equal))
                                (dia (+ 1 (random 7)))
                                (hora (+ 1 (random 10)))
                                (id (concatenate 'string "t" (write-to-string (random 999)))))
                            (when (not (gethash (cons dia hora) hash))
                              (setf (gethash (cons dia hora) hash) id)
                              (setf alt (append alt (list (faz-subtarefa :dia dia :hora hora :id id)))))))
                    (setf tarefa (append tarefa (list alt)))))
            (setf result (append result (list tarefa)))))
    result))

(defun dump-test (teste)
  (dolist (tarefa teste)
    (format t "~2&Tarefa:~&")
    (dolist (alt tarefa)
      (format t "~6TAlternativa:~&")
      (dolist (sub alt)
        (format t "~12T~{\"~A\" ~}\~&" sub)))))

(defvar tarefas
  (list ; lista de tarefas
    (list ; lista de alternativas
      (list ; lista de subtarefas
        (faz-subtarefa :dia 1 :hora 9 :id 't121)
        (faz-subtarefa :dia 1 :hora 10 :id 't122))
      (list
        (faz-subtarefa :dia 2 :hora 10 :id 't123)
        (faz-subtarefa :dia 2 :hora 11 :id 't124)))
    (list
      (list
        (faz-subtarefa :dia 1 :hora 9 :id 't121)
        (faz-subtarefa :dia 1 :hora 11 :id 't132))
      (list
        (faz-subtarefa :dia 2 :hora 11 :id 't133)
        (faz-subtarefa :dia 2 :hora 12 :id 't134)))
    ))

(defvar problema (formulacao-problema tarefas))
(defvar problema1 (formulacao-problema1 tarefas))

(defparameter *ex1*
  (list
   (list
    (list
     (faz-subtarefa :dia 1 :hora 9 :id 't101)
     (faz-subtarefa :dia 1 :hora 10 :id 't102)
     (faz-subtarefa :dia 1 :hora 12 :id 't103))
    (list 
     (faz-subtarefa :dia 2 :hora 9 :id 't111)
     (faz-subtarefa :dia 2 :hora 10 :id 't112)
     (faz-subtarefa :dia 4 :hora 10 :id 't115)
     (faz-subtarefa :dia 4 :hora 12 :id 't116))
    (list 
     (faz-subtarefa :dia 2 :hora 9 :id 't121)
     (faz-subtarefa :dia 2 :hora 12 :id 't123))
    )
   (list
    (list
     (faz-subtarefa :dia 1 :hora 9 :id 't101)
     (faz-subtarefa :dia 1 :hora 11 :id 't222)
     (faz-subtarefa :dia 1 :hora 12 :id 't103))
    (list 
     (faz-subtarefa :dia 1 :hora 13 :id 't231)
     (faz-subtarefa :dia 1 :hora 14 :id 't232)
     (faz-subtarefa :dia 1 :hora 15 :id 't233))
    (list 
     (faz-subtarefa :dia 1 :hora 14 :id 't231)
     (faz-subtarefa :dia 1 :hora 15 :id 't232)
     (faz-subtarefa :dia 1 :hora 16 :id 't233))
    )))
(defvar p1 (formulacao-problema *ex1*))
(defvar p2 (formulacao-problema1 *ex1*))

(defparameter ex5

  (list ;de tarefas
    (list
      (list (faz-subtarefa :dia 1 :hora 9 :id 't1))
      (list (faz-subtarefa :dia 2 :hora 9 :id 't2))
      (list (faz-subtarefa :dia 3 :hora 9 :id 't3))
      (list (faz-subtarefa :dia 1 :hora 10 :id 't4))
      (list (faz-subtarefa :dia 2 :hora 10 :id 't5))
      (list (faz-subtarefa :dia 3 :hora 10 :id 't6))
      (list (faz-subtarefa :dia 1 :hora 11 :id 't7))
      (list (faz-subtarefa :dia 2 :hora 11 :id 't8))
      (list (faz-subtarefa :dia 3 :hora 11 :id 't9)))
    (list
      (list (faz-subtarefa :dia 1 :hora 19 :id 't01))
      (list (faz-subtarefa :dia 2 :hora 19 :id 't02))
      (list (faz-subtarefa :dia 3 :hora 19 :id 't03)))
    (list
      (list (faz-subtarefa :dia 3 :hora 10 :id 't11))
      (list (faz-subtarefa :dia 4 :hora 10 :id 't12))
      (list (faz-subtarefa :dia 5 :hora 10 :id 't13)))
    (list
      (list (faz-subtarefa :dia 3 :hora 11 :id 't21))
      (list (faz-subtarefa :dia 4 :hora 11 :id 't22))
      (list (faz-subtarefa :dia 5 :hora 11 :id 't23)))
    (list
      (list (faz-subtarefa :dia 3 :hora 12 :id 't31))
      (list (faz-subtarefa :dia 4 :hora 12 :id 't32))
      (list (faz-subtarefa :dia 5 :hora 12 :id 't33)))
    (list
      (list (faz-subtarefa :dia 3 :hora 13 :id 't41))
      (list (faz-subtarefa :dia 4 :hora 13 :id 't42))
      (list (faz-subtarefa :dia 5 :hora 13 :id 't43)))
    (list
      (list (faz-subtarefa :dia 3 :hora 14 :id 't51))
      (list (faz-subtarefa :dia 4 :hora 14 :id 't52))
      (list (faz-subtarefa :dia 5 :hora 14 :id 't53)))
    #|   (list (list (faz-subtarefa :dia 3 :hora 15 :id 't61))
               (list (faz-subtarefa :dia 4 :hora 15 :id 't62))
               (list (faz-subtarefa :dia 5 :hora 15 :id 't63)))
    (list
      (list (faz-subtarefa :dia 3 :hora 16 :id 't71))
      (list (faz-subtarefa :dia 4 :hora 16 :id 't72))
      (list (faz-subtarefa :dia 5 :hora 16 :id 't73)))
    (list
      (list (faz-subtarefa :dia 3 :hora 17 :id 't81))
      (list (faz-subtarefa :dia 4 :hora 17 :id 't82))
      (list (faz-subtarefa :dia 5 :hora 17 :id 't83)))
    (list
      (list (faz-subtarefa :dia 3 :hora 18 :id 't91))
      (list (faz-subtarefa :dia 4 :hora 18 :id 't92))
      (list (faz-subtarefa :dia 5 :hora 18 :id 't93)))
    |#   
    (list
      (list (faz-subtarefa :dia 1 :hora 9 :id 't101) 
            (faz-subtarefa :dia 2 :hora 9 :id 't102)
            (faz-subtarefa :dia 3 :hora 9 :id 't103)
            (faz-subtarefa :dia 1 :hora 10 :id 't104)
            ;(faz-subtarefa :dia 2 :hora 10 :id 't105)
            (faz-subtarefa :dia 3 :hora 10 :id 't106)
            (faz-subtarefa :dia 1 :hora 11 :id 't107)
            (faz-subtarefa :dia 2 :hora 11 :id 't108)
            (faz-subtarefa :dia 3 :hora 11 :id 't109)))))

(defvar problema-ex5 (formulacao-problema ex5))
(defvar problema-ex51 (formulacao-problema1 ex5))

(defparameter ex6
  (list ;de tarefas
    (list
      (list (faz-subtarefa :dia 1 :hora 9 :id 't101)
            (faz-subtarefa :dia 2 :hora 9 :id 't102)
            (faz-subtarefa :dia 3 :hora 9 :id 't103)
            (faz-subtarefa :dia 1 :hora 10 :id 't104)
            ;(faz-subtarefa :dia 2 :hora 10 :id 't105)
            (faz-subtarefa :dia 3 :hora 10 :id 't106)
            (faz-subtarefa :dia 1 :hora 11 :id 't107)
            (faz-subtarefa :dia 2 :hora 11 :id 't108)
            (faz-subtarefa :dia 3 :hora 11 :id 't109)))
    (list
      (list (faz-subtarefa :dia 1 :hora 19 :id 't01))
      (list (faz-subtarefa :dia 2 :hora 19 :id 't02))
      (list (faz-subtarefa :dia 3 :hora 19 :id 't03)))
    (list
      (list (faz-subtarefa :dia 3 :hora 10 :id 't11))
      (list (faz-subtarefa :dia 4 :hora 10 :id 't12))
      (list (faz-subtarefa :dia 5 :hora 10 :id 't13)))
    (list 
      (list (faz-subtarefa :dia 3 :hora 11 :id 't21))
      (list (faz-subtarefa :dia 4 :hora 11 :id 't22))
      (list (faz-subtarefa :dia 5 :hora 11 :id 't23)))
    (list
      (list (faz-subtarefa :dia 3 :hora 12 :id 't31))
      (list (faz-subtarefa :dia 4 :hora 12 :id 't32))
      (list (faz-subtarefa :dia 5 :hora 12 :id 't33)))
    (list
      (list (faz-subtarefa :dia 3 :hora 13 :id 't41))
      (list (faz-subtarefa :dia 4 :hora 13 :id 't42))
      (list (faz-subtarefa :dia 5 :hora 13 :id 't43)))
    (list
      (list (faz-subtarefa :dia 3 :hora 14 :id 't51))
      (list (faz-subtarefa :dia 4 :hora 14 :id 't52))
      (list (faz-subtarefa :dia 5 :hora 14 :id 't53)))
    (list
      (list (faz-subtarefa :dia 3 :hora 15 :id 't61))
      (list (faz-subtarefa :dia 4 :hora 15 :id 't62))
      (list (faz-subtarefa :dia 5 :hora 15 :id 't63)))
    (list
      (list (faz-subtarefa :dia 3 :hora 16 :id 't71))
      (list (faz-subtarefa :dia 4 :hora 16 :id 't72))
      (list (faz-subtarefa :dia 5 :hora 16 :id 't73)))
    (list
      (list (faz-subtarefa :dia 3 :hora 17 :id 't81))
      (list (faz-subtarefa :dia 4 :hora 17 :id 't82))
      (list (faz-subtarefa :dia 5 :hora 17 :id 't83)))
    (list
      (list (faz-subtarefa :dia 3 :hora 18 :id 't91))
      (list (faz-subtarefa :dia 4 :hora 18 :id 't92))
      (list (faz-subtarefa :dia 5 :hora 18 :id 't93)))
    (list
      (list (faz-subtarefa :dia 1 :hora 9 :id 't1))
      (list (faz-subtarefa :dia 2 :hora 9 :id 't2))
      (list (faz-subtarefa :dia 3 :hora 9 :id 't3))
      (list (faz-subtarefa :dia 1 :hora 10 :id 't4))
      (list (faz-subtarefa :dia 2 :hora 10 :id 't5))
      (list (faz-subtarefa :dia 3 :hora 10 :id 't6))
      (list (faz-subtarefa :dia 1 :hora 11 :id 't7))
      (list (faz-subtarefa :dia 2 :hora 11 :id 't8))
      (list (faz-subtarefa :dia 3 :hora 11 :id 't9)))
    ))

(defvar problema-ex6 (formulacao-problema ex6))
(defvar problema-ex61 (formulacao-problema1 ex6))

(defparameter omfg

  (list ;de tarefas
    (list
      (list (faz-subtarefa :dia 1 :hora 9 :id 't1))
      (list (faz-subtarefa :dia 2 :hora 9 :id 't2))
      (list (faz-subtarefa :dia 3 :hora 9 :id 't3))
      (list (faz-subtarefa :dia 1 :hora 10 :id 't4))
      (list (faz-subtarefa :dia 2 :hora 10 :id 't5))
      (list (faz-subtarefa :dia 3 :hora 10 :id 't6))
      (list (faz-subtarefa :dia 1 :hora 11 :id 't7))
      (list (faz-subtarefa :dia 2 :hora 11 :id 't8))
      (list (faz-subtarefa :dia 3 :hora 11 :id 't9)))
    (list 
      (list (faz-subtarefa :dia 1 :hora 19 :id 't01))
      (list (faz-subtarefa :dia 2 :hora 19 :id 't02))
      (list (faz-subtarefa :dia 3 :hora 19 :id 't03)))
    (list 
      (list (faz-subtarefa :dia 3 :hora 10 :id 't11))
      (list (faz-subtarefa :dia 4 :hora 10 :id 't12))
      (list (faz-subtarefa :dia 5 :hora 10 :id 't13)))
    (list 
      (list (faz-subtarefa :dia 3 :hora 11 :id 't21))
      (list (faz-subtarefa :dia 4 :hora 11 :id 't22))
      (list (faz-subtarefa :dia 5 :hora 11 :id 't23)))
    (list
      (list (faz-subtarefa :dia 3 :hora 12 :id 't31))
      (list (faz-subtarefa :dia 4 :hora 12 :id 't32))
      (list (faz-subtarefa :dia 5 :hora 12 :id 't33)))
    (list 
      (list (faz-subtarefa :dia 3 :hora 13 :id 't41))
      (list (faz-subtarefa :dia 4 :hora 13 :id 't42))
      (list (faz-subtarefa :dia 5 :hora 13 :id 't43)))
    (list 
      (list (faz-subtarefa :dia 3 :hora 14 :id 't51))
      (list (faz-subtarefa :dia 4 :hora 14 :id 't52))
      (list (faz-subtarefa :dia 5 :hora 14 :id 't53)))
    (list 
      (list (faz-subtarefa :dia 3 :hora 15 :id 't61))
      (list (faz-subtarefa :dia 4 :hora 15 :id 't62))
      (list (faz-subtarefa :dia 5 :hora 15 :id 't63)))
    (list
      (list (faz-subtarefa :dia 3 :hora 16 :id 't71))
      (list (faz-subtarefa :dia 4 :hora 16 :id 't72))
      (list (faz-subtarefa :dia 5 :hora 16 :id 't73)))
    (list 
      (list (faz-subtarefa :dia 3 :hora 17 :id 't81))
      (list (faz-subtarefa :dia 4 :hora 17 :id 't82))
      (list (faz-subtarefa :dia 5 :hora 17 :id 't83)))
    (list 
      (list (faz-subtarefa :dia 3 :hora 18 :id 't91))
      (list (faz-subtarefa :dia 4 :hora 18 :id 't92))
      (list (faz-subtarefa :dia 5 :hora 18 :id 't93)))   
    (list
      (list (faz-subtarefa :dia 1 :hora 9 :id 't101) 
            (faz-subtarefa :dia 2 :hora 9 :id 't102)
            (faz-subtarefa :dia 3 :hora 9 :id 't103)
            (faz-subtarefa :dia 1 :hora 10 :id 't104)
            (faz-subtarefa :dia 2 :hora 10 :id 't105)
            (faz-subtarefa :dia 3 :hora 10 :id 't106)
            (faz-subtarefa :dia 1 :hora 11 :id 't107)
            (faz-subtarefa :dia 2 :hora 11 :id 't108)
            (faz-subtarefa :dia 3 :hora 11 :id 't109)))))


(defparameter omg

  (list ;de tarefas
    (list
      (list (faz-subtarefa :dia 1 :hora 9 :id 't1))
      (list (faz-subtarefa :dia 2 :hora 9 :id 't2))
      (list (faz-subtarefa :dia 3 :hora 9 :id 't3))
      (list (faz-subtarefa :dia 1 :hora 10 :id 't4))
      (list (faz-subtarefa :dia 2 :hora 10 :id 't5))
      (list (faz-subtarefa :dia 3 :hora 10 :id 't6))
      (list (faz-subtarefa :dia 1 :hora 11 :id 't7))
      (list (faz-subtarefa :dia 2 :hora 11 :id 't8))
      (list (faz-subtarefa :dia 3 :hora 11 :id 't9)))
    (list
      (list (faz-subtarefa :dia 1 :hora 19 :id 't01))
      (list (faz-subtarefa :dia 2 :hora 19 :id 't02))
      (list (faz-subtarefa :dia 3 :hora 19 :id 't03)))
    (list
      (list (faz-subtarefa :dia 3 :hora 10 :id 't11))
      (list (faz-subtarefa :dia 4 :hora 10 :id 't12))
      (list (faz-subtarefa :dia 5 :hora 10 :id 't13)))
    (list
      (list (faz-subtarefa :dia 3 :hora 11 :id 't21))
      (list (faz-subtarefa :dia 4 :hora 11 :id 't22))
      (list (faz-subtarefa :dia 5 :hora 11 :id 't23)))
    (list
      (list (faz-subtarefa :dia 3 :hora 12 :id 't31))
      (list (faz-subtarefa :dia 4 :hora 12 :id 't32))
      (list (faz-subtarefa :dia 5 :hora 12 :id 't33)))
    (list
      (list (faz-subtarefa :dia 3 :hora 13 :id 't41))
      (list (faz-subtarefa :dia 4 :hora 13 :id 't42))
      (list (faz-subtarefa :dia 5 :hora 13 :id 't43)))
    (list
      (list (faz-subtarefa :dia 3 :hora 14 :id 't51))
      (list (faz-subtarefa :dia 4 :hora 14 :id 't52))
      (list (faz-subtarefa :dia 5 :hora 14 :id 't53)))
   (list (list (faz-subtarefa :dia 3 :hora 15 :id 't61))
               (list (faz-subtarefa :dia 4 :hora 15 :id 't62))
               (list (faz-subtarefa :dia 5 :hora 15 :id 't63)))
    (list
      (list (faz-subtarefa :dia 3 :hora 16 :id 't71))
      (list (faz-subtarefa :dia 4 :hora 16 :id 't72))
      (list (faz-subtarefa :dia 5 :hora 16 :id 't73)))
    (list
      (list (faz-subtarefa :dia 3 :hora 17 :id 't81))
      (list (faz-subtarefa :dia 4 :hora 17 :id 't82))
      (list (faz-subtarefa :dia 5 :hora 17 :id 't83)))
    (list
      (list (faz-subtarefa :dia 3 :hora 18 :id 't91))
      (list (faz-subtarefa :dia 4 :hora 18 :id 't92))
      (list (faz-subtarefa :dia 5 :hora 18 :id 't93)))
   
    (list
      (list (faz-subtarefa :dia 1 :hora 9 :id 't101) 
            (faz-subtarefa :dia 2 :hora 9 :id 't102)
            (faz-subtarefa :dia 3 :hora 9 :id 't103)
            (faz-subtarefa :dia 1 :hora 10 :id 't104)
            ;(faz-subtarefa :dia 2 :hora 10 :id 't105)
            (faz-subtarefa :dia 3 :hora 10 :id 't106)
            (faz-subtarefa :dia 1 :hora 11 :id 't107)
            (faz-subtarefa :dia 2 :hora 11 :id 't108)
            (faz-subtarefa :dia 3 :hora 11 :id 't109)))))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
