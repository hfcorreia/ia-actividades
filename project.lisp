;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Estruturas
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defstruct problema
  estado-inicial solucao? accoes resultado custo-caminho)

(defstruct no
  estado pai accao nivel custo-caminho heuristica avaliacao)

(defstruct csp 
  variaveis dominios restricoes)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Funcoes de print
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun print-hash (key value)
  (format t "~&Key: ~D VAL: ~D~&" key value))

(defun dump (lista limite)
  (let ((counter 0))
    (dolist (no lista)
      (format t "~2&No ~D~&Custo: ~D~&Limite: ~D~&Nivel: ~D~&Avaliacao: ~D~&" counter (no-custo-caminho no) limite (no-nivel no) (no-avaliacao no))
      (incf counter)
      (dolist (estado-lst (no-estado no))
        (format t "~10T~{~A~}~&" estado-lst)))
    (princ "============================")))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Formulacoes
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun faz-subtarefa (&key dia hora id)
  ;Devolve uma lista com dia data e hora.
  (list dia hora id))

;;; Funcao formulacao-problema
(defun formulacao-problema (lista-tarefas)
  ;Cria um novo problema baseado numa lista de tarefas
  (make-problema
    :estado-inicial '()

    :solucao?
    (lambda (estado)
      (if (= (length estado) (length lista-tarefas))
        t
        nil))
;Apenas uma tarefa em cada nivel
    :accoes
    (lambda (estado)
      (let ((accoes nil)
            (tabela (make-hash-table :test #'equal))
            (tarefa (nth (length estado) lista-tarefas)))

        (if (not (null tarefa))
          (progn
            (dolist (alt estado)
              (dolist (sub alt)
                (setf (gethash (cons (first sub) (second sub)) tabela) (third sub))))

            (dolist (alt tarefa)
              (block subtarefas
                (dolist (sub alt)
                  (if (gethash (cons (first sub) (second sub)) tabela)
                    (when (not (equal (third sub) (gethash (cons (first sub) (second sub)) tabela)))
                      (return-from subtarefas nil))))
                (setf accoes (append accoes (list alt)))))
            accoes)
          nil)))

    :resultado
    (lambda (estado accao)
      (let ((novo-estado nil))
        (setf 
          novo-estado (append (list accao) estado))
        novo-estado))

    :custo-caminho 
    (lambda (estado accao)
      (let ((valor 0)
            (tabela (make-hash-table :test #'equal)))
        (if (not (null estado))
          (progn
            (dolist (alt estado)
              (dolist (sub alt)
                (setf (gethash (cons (first sub)
                                     (second sub))
                               tabela)
                      (third sub))))

            (dolist (sub accao)
              (if (not (gethash (cons (first sub) (second sub)) tabela))
                (incf valor))))
          (setf valor (length accao)))
        valor))))


(defun no-filho (problema no-pai accao)
  (make-no
    :estado (funcall (problema-resultado problema) (no-estado no-pai) accao)
    :pai no-pai
    :accao accao
    :nivel (+ (no-nivel no-pai) 1)
    :custo-caminho (+ (no-custo-caminho no-pai) (funcall (problema-custo-caminho problema) (no-estado no-pai) accao))
    :heuristica 0
    :avaliacao 0))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Procuras
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun procura-profundidade (problema no-folha)
  (let ((nos-gerados nil)
        (no-gerado nil))
    (dolist (accao (funcall (problema-accoes problema) (no-estado no-folha)))
      (setf no-gerado (no-filho problema no-folha accao))
      (setf nos-gerados (append (list no-gerado) nos-gerados)))
    (list nil nos-gerados)))

(defun procura-largura (problema no-folha)
  (let ((nos-gerados nil)
        (no-gerado nil))
    (block looping
           (dolist (accao (funcall (problema-accoes problema) (no-estado no-folha)))
             (setf no-gerado (no-filho problema no-folha accao))
             (when (funcall (problema-solucao? problema) (no-estado no-gerado))
               (return-from looping (list no-gerado nil)))
             (setf nos-gerados (append nos-gerados (list no-gerado))))
           (list nil nos-gerados))))

(defun troca-no (no-gerado fronteira inserir)
  (let ((lista nil)
        (estado-gerado (no-estado no-gerado))
        (encontrou? nil))
    (dolist (no fronteira)
      (if (not (equal (no-estado no) estado-gerado))
        (setf lista (append lista (list no)))
        (setf encontrou? t)))
    (when (equal encontrou? t)
      (funcall inserir no-gerado lista))
    lista))

(defun remove-no (no-gerado fronteira)
  (let ((lista nil)
        (estado-gerado (no-estado no-gerado)))
    (dolist (no fronteira)
      (if (not (equal (no-estado no) estado-gerado))
        (setf lista (append lista (list no)))))
    lista))


(defun ver-outros (problema fronteira inserir tipo-procura limite)
  (let ((no-folha nil)
        (nivel-folha nil)
        (estado-folha nil)
        (resultado-procura nil))
    (loop do
          (if (null fronteira)
            (return 'cutoff)
            (progn
              (setf no-folha (pop fronteira))
              (setf nivel-folha (no-nivel no-folha))
              (cond ((= nivel-folha limite)
                     (setf estado-folha (no-estado no-folha))
                     (when (funcall (problema-solucao? problema) estado-folha) 
                       (return estado-folha)))
                    ((< nivel-folha limite)
                     (setf estado-folha (no-estado no-folha))
                     (when (funcall (problema-solucao? problema) estado-folha) 
                       (return estado-folha))
                     (setf resultado-procura (funcall tipo-procura problema no-folha))
                     (if (null (first resultado-procura))
                       (dolist (no (second resultado-procura))
                         (setf fronteira (funcall inserir no fronteira)))
                       (return (no-estado (first resultado-procura)))))
                    (t nil)))))))

(defun avaliacao (no &key heuristica custo)
    (cond ((and (not (null custo)) (not (null heuristica)))
      (setf (no-heuristica no) (funcall heuristica no))
     (setf (no-avaliacao no) (+ (no-custo-caminho no) (no-heuristica no))))
    ((not (null custo))
     (setf (no-avaliacao no) (no-custo-caminho no)))
    ((not (null heuristica))
      (setf (no-heuristica no) (funcall heuristica no))
     (setf (no-avaliacao no) (no-heuristica no))))
    no)

(defun procura-arvore (problema inserir tipo-procura &key (limite most-positive-fixnum) custo largura heuristica custo-caminho gananciosa)
  (let ((fronteira (list (make-no :estado (problema-estado-inicial problema) :nivel 0 :custo-caminho 0)))
    (no-folha nil)
    (tabela (make-hash-table :test #'equal))
    (resultado-procura nil)
    (verifica (not largura))
    (organizada nil)
    (encontrado nil))

    (setf (gethash (problema-estado-inicial problema) tabela) 0)

    (if (= limite 0)
      'cutoff
      (block teste-limite
        (setf (first fronteira) (avaliacao (first fronteira) :heuristica heuristica :custo custo-caminho))
       (dotimes (n limite)   
         (when (null fronteira) 
           (return nil))

         (setf no-folha (pop fronteira))
         (remhash (no-estado no-folha) tabela)

         (if (not (equal largura verifica))
          (progn
            (when (funcall (problema-solucao? problema) (no-estado no-folha))
              (return-from teste-limite (no-estado no-folha)))
            (setf verifica t)))

         (setf resultado-procura (funcall tipo-procura problema no-folha)) 

         (if (null (first resultado-procura))
           (progn
             (dolist (no (second resultado-procura))
               (setf no (avaliacao no :heuristica heuristica :custo custo-caminho))
               (if gananciosa
                 (setf organizada (funcall inserir no organizada))
                 (setf fronteira (funcall inserir no fronteira)))
               (when custo
                (setf encontrado (gethash (no-estado no) tabela))
                (when encontrado
                 (if (< (no-avaliacao no) (no-avaliacao encontrado))
                  (if gananciosa
                    (progn
                      (setf fronteira (remove-no encontrado fronteira))
                      (setf (gethash (no-estado encontrado) tabela) (no-avaliacao encontrado)))
                    (progn
                      (setf fronteira (troca-no no fronteira inserir))
                      (setf (gethash (no-estado no) tabela) (no-avaliacao no))))))))
             (when gananciosa
               (setf fronteira (append organizada fronteira))
               (setf organizada nil)))
           (return-from teste-limite (no-estado (first resultado-procura))))

         (when (= (+ n 1) limite) 
           (return-from teste-limite (ver-outros problema fronteira inserir tipo-procura limite))))))))

(defun procura-grafo (problema inserir tipo-procura &key (limite most-positive-fixnum) custo largura heuristica custo-caminho gananciosa)
  (let ((fronteira (list (make-no :estado (problema-estado-inicial problema) :nivel 0 :custo-caminho 0)))
        (explorados (make-hash-table :test #'equal))
        (tabela (make-hash-table :test #'equal))
        (no-folha nil)
        (resultado-procura nil)
        (estado-no-gerado nil)
        (verifica (not largura))
        (encontrado nil)
        (organizada nil))

    (setf (gethash (problema-estado-inicial problema) tabela) 0)

    (if (= limite 0)
      'cutoff
      (block teste-limite
        (setf (first fronteira) (avaliacao (first fronteira) :heuristica heuristica :custo custo-caminho))
             (dotimes (n limite)   
               (when (null fronteira) 
                 (return nil))

               (setf no-folha (pop fronteira))
               (remhash (no-estado no-folha) tabela)

               (if (not (equal largura verifica))
                (progn
                 (when (funcall (problema-solucao? problema) (no-estado no-folha))
                   (return-from teste-limite (no-estado no-folha)))
                 (setf verifica t)))

               (setf (gethash (no-estado no-folha) explorados) (no-avaliacao no-folha))

               (setf resultado-procura (funcall tipo-procura problema no-folha))

               (if (null (first resultado-procura))
                 (progn
                   (dolist (no (second resultado-procura))
                     (setf no (avaliacao no :heuristica heuristica :custo custo-caminho))
                     (setf estado-no-gerado (no-estado no))
                     (if (and (null (gethash estado-no-gerado explorados)) (null (gethash estado-no-gerado tabela)))
                       (if gananciosa
                         (setf organizada (funcall inserir no organizada))
                         (setf fronteira (funcall inserir no fronteira)))
                       (when custo
                          (setf encontrado (gethash (no-estado no) tabela))
                          (when encontrado
                         (if (< (no-avaliacao no) (no-avaliacao encontrado))
                           (if gananciosa
                             (progn
                               (setf fronteira (remove-no no fronteira))
                               (setf (gethash (no-estado encontrado) tabela) (no-avaliacao encontrado)))
                             (progn
                               (setf fronteira (troca-no no fronteira inserir))
                               (setf (gethash (no-estado no) tabela) (no-avaliacao no)))))))))
                   (if gananciosa
                     (setf fronteira (append organizada fronteira))
                     (setf organizada nil)))
                  (return-from teste-limite (no-estado (first resultado-procura))))


               (when (= (+ n 1) limite) 
                 (return-from teste-limite (ver-outros problema fronteira inserir tipo-procura limite))))))))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Load all files
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(load "queue.lisp")
(load "stack.lisp")
(load "heuristics.lisp")
(load "searches.lisp")
(load "problems.lisp")
(load "test.lisp")
(load "csp.lisp")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; (defun profs ()(sb-profile:profile 
;                   print-hash
;                   dump
;                   faz-subtarefa
;                   formulacao-problema
;                   no-filho
;                   procura-profundidade
;                   procura-largura
;                   troca-no
;                   ver-outros
;                   procura-arvore
;                   procura-grafo
;                   pilha-inserir
;                   pilha-inserir-ordenado
;                   pilha-existe-elemento?
;                   fila-inserir
;                   fila-inserir-ordenado
;                   fila-existe-elemento?
;                   pppa
;                   pppg
;                   plpa
;                   plpg
;                   ppla
;                   pplg
;                   ppia
;                   ppig
;                   pcua
;                   pcug
;                   pga
;                   pgg
;                   pA*a
;                   pA*g
;                   rbfs
;                   psr
;                   heuristica-total
;                   heuristica-horas
;                   avaliacao))

