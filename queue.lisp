;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Queue functions
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun fila-inserir (gerado lista)
  (append lista (list gerado)))

(defun fila-inserir-ordenado (elemento lista)
  (cond ((null lista)
          (list elemento))
        ((>= (no-custo-caminho (car lista)) (no-custo-caminho elemento))
          (cons elemento (cons (car lista) (cdr lista))))
        ((< (no-custo-caminho (car lista)) (no-custo-caminho elemento))
          (cons (car lista) (fila-inserir-ordenado elemento (cdr lista))))))

(defun fila-existe-elemento? (elemento lista)
  (cond ((null lista) nil)
        ((equalp (no-estado (car lista)) (no-estado elemento)) t)
        (t (fila-existe-elemento? elemento (cdr lista)))))
